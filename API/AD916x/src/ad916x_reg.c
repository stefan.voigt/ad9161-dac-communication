#include "api_config.h"
#include <stdint.h>
#include "AD916x.h"
#include "ad916x_reg.h"
#include "api_errors.h"
#include "utils.h"

#define IN_OUT_BUFF_SZ 3

ADI_API int ad916x_register_write(ad916x_handle_t *h, 
								const uint16_t address, const uint8_t data)
{
	int err = API_ERROR_INVALID_HANDLE_PTR;
	uint8_t inData[IN_OUT_BUFF_SZ];
	uint8_t outData[IN_OUT_BUFF_SZ];

	if (h == INVALID_POINTER) {
		return API_ERROR_INVALID_HANDLE_PTR;
	}
	if (h->dev_xfer == INVALID_POINTER) {
		return API_ERROR_INVALID_XFER_PTR;
	}
	inData[0] = ((address >> 8) & 0xFF);
	inData[1] = ((address >> 0) & 0xFF);
	inData[2] = data;
	err = h->dev_xfer(h->user_data, inData, outData, IN_OUT_BUFF_SZ);
	if (err != 0)
	{
		return API_ERROR_SPI_XFER;
	}
	return API_ERROR_OK;
}

ADI_API int ad916x_register_read(ad916x_handle_t *h, 
								const uint16_t address, uint8_t *data)
{
	int err = API_ERROR_INVALID_HANDLE_PTR;
	uint8_t inData[IN_OUT_BUFF_SZ];
	uint8_t outData[IN_OUT_BUFF_SZ];

	if (h == INVALID_POINTER) {
		return API_ERROR_INVALID_HANDLE_PTR;
	}
	if (h->dev_xfer == INVALID_POINTER) {
		return API_ERROR_INVALID_XFER_PTR;
	}
	/* set the 'read' bit. */
	inData[0] = (((address | 0x8000) >> 8) & 0xFF) ;
	inData[1] = ((address >> 0) & 0xFF);
	err = h->dev_xfer(h->user_data, inData, outData, IN_OUT_BUFF_SZ);
	if (err == 0) {
		*data = outData[2];
	} else {
		return API_ERROR_SPI_XFER;
	}
	return API_ERROR_OK;
}

int ad916x_register_write_tbl(ad916x_handle_t *h, struct
							ad916x_reg_data *tbl, uint32_t count)
{
	uint16_t i =0;

	if (h == INVALID_POINTER) {
		return API_ERROR_INVALID_HANDLE_PTR;
	}
	for (i = 0; i<count; i++) {
		ad916x_register_write(h, tbl[i].reg, tbl[i].val);
	}

	return API_ERROR_OK;
}
