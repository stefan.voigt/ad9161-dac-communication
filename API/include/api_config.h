#ifndef __API_CONFIG_H
#define __API_CONFIG_H

#define DYNAMIC_LIBRARY 0
#define LIBRARY_EXPORTS 0

/* Specify the invalid pointer address. */
#define INVALID_POINTER 0

/* Uncomment 'BUILD_ERROR_DESCRIPTION' if you want to build the error string description API function. */
/* #define BUILD_ERROR_DESCRIPTION */

#ifdef __cplusplus
#define ADI_API extern "C" 
#else#define ADI_API
#endif /* __cplusplus */

#if   0
#ifdef _WIN32
#if (DYNAMIC_LIBRARY != 0)
#ifdef (LIBRARY_EXPORTS != 0)
#define ADI_API __declspec(dllexport)
#else
#define ADI_API __declspec(dllimport)
#endif
#else if/*Static library win*/#define ADI_API
#endif
#else
/*Static library nowin*/
#define ADI_API
#endif
#endif
	
#endif /* !__API_CONFIG_H */
