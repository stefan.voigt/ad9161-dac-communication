# AD9161 DAC communication

## Build and run instructions
### 1. Compile the modified AD916x Library
1. Move to the directory `API/AD916x`
2. Run `make`
### 2. Compile our control software
1. Move to the directory `Applications/ad916x_dac_example`
2. Run `make BOARD=GENESYS2 DAC=AD9161` for our setup
### 3. Run our control software
1. Make sure, that the FPGA has the matching bitfile programmed and that a USB-Cable is attached from the UART to the Host.
2. Start the compiled appication by executing `debug/./ad916xDacApp` in from the previously used compile directory `Applications/ad916x_dac_example`.
3. If all goes well, you will get an output simmilar to this:

```
APP: DAC Module JESD Configuration Application 
APP:INIT: SPI 
INIT UART
== GOT AN ALIVE BACK FROM THE FPGA! ==
APP:INIT: Clock 
CLK:DAC RATE: 5000 MHz 
CLK:Mode: Disable Output for 0,1,2
CLK:Mode: Enable Output 3 for AD9161, Sysref
CLK:Mode: Div Ratio final: 3
APP:INIT: DAC 
AD916x: dac_init called
DAC set spi mode
DAC INIT SEQUENCE
ad916x_init okay
*********************************************
AD916x DAC Chip ID: 4 
AD916x DAC Product ID: 9161 
AD916x DAC Product Grade: 0 
AD916x DAC Product Revision: 3 
AD916x Revision: 1.0.0 
*********************************************
**** Command Menu******* 
j: Configure JESD MODE 
n: Configure NCO Shift
e: Enable MODE 
s: Check Mode Status 
q: Quit Application 
******************** 
```
