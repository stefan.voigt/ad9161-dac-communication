/**
 * \file dac_cfg.h
 *
 * \brief Defines the Application - DAC Module Interface
 *
 */

#ifndef DAC_CFG_H_
#define DAC_CFG_H_

#ifdef AD9164
#include "AD9164.h"
#else
#include "AD916x.h"
#endif


#ifdef AD9164
extern ad9164_handle_t dac_h;
#else
extern ad916x_handle_t dac_h;
#endif

typedef enum {
	DAC_JESD_MODE = 0,
	DAC_NCO_ONLY_MODE,
	DAC_NCO_SHIFT
}DAC_MODE;

typedef struct {
	int interpol;
	int L;
	int M;
	int F;
	int S;
}dac_jesd_data;

typedef struct {
    uint64_t carrier_freq;
    uint16_t amplitude;
}dac_nco_data;



int dac_init(void* dac_h);
int dac_reset(void* dac_h);
int dac_shutdown(void* dac_h);
int dac_set_mode(void* dac_h, DAC_MODE dac_mode, int dac_rate_mhz, void *mode_data);
int dac_enable_mode(void* dac_h);
int dac_enable_mode_repeated_ILAS(void* dac_h);
int dac_monitor(void* dach_h);
int dac_event_handler(uint16_t event, uint8_t ref, void* data);


/*debug Functions*/
int dac_check_init(void* dac_h);
int dac_check_jesd_config(void* dac_h);
int dac_check_int_config(void* dac_h);
int dac_check_nco_config(void* dac_h);
#endif
