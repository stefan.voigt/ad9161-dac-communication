/**
 * \brief Contains Application Configuration Options
 *
 */

/*Configure the following Application Options*/
#define ON_BOARD_CLK  1   
#define EXT_REF_CLK_MHZ   120
#define DAC_RATE_MHZ      5000




/*Do not modify*/
#if (DAC_RATE_MHZ == 2000)
#define DEF_LANE_RATE_MHZ 5000
#elif (DAC_RATE_MHZ == 6000)
#define DEF_LANE_RATE_MHZ 10000
#elif (DAC_RATE_MHZ == 5000)
#define DEF_LANE_RATE_MHZ 12500
#else
#define DEF_LANE_RATE_MHZ 5000
#endif
