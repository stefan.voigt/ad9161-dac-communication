/**
 * \file main.c
 *
 * \brief Contains main function for example application 
 *        Example application is example code for user 
 *        integration and use of DAC module 
 *
 */
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#if BB
#include <linux/input.h>
#endif
#include "app.h"

#define MAX_CMD_BUFFER 2 
static int currAppMode;


/*******************************/
/****DAC Example APP Main ***/
/*******************************/


int main()
{
	int fd;
	const char *pDevice = "/dev/tty";
	int quit_flag= 0;
	int char_count = 0;
	char buffer[MAX_CMD_BUFFER];

	/*Access Terminal*/
	fd = open(pDevice, O_RDONLY | O_NONBLOCK);
	if (fd == -1) {
	   printf("APP:Error accessing command terminal");
	   return -1;
	}

	printf("APP: DAC Module JESD Configuration Application \r\n");
	/*Initialise Application*/
	currAppMode = 0;
	APP_init();


	printf("**** Command Menu******* \r\n");
#if (!AD9161)
	printf("t: Configure DC Test Mode (NCO Only)\r\n");
#endif	
	printf("j: Configure JESD MODE \r\n");
	printf("n: Configure NCO Shift\r\n");
	printf("e: Enable MODE \r\n");
	printf("s: Check Mode Status \r\n");
	printf("p: PRBS Checker\r\n");
	printf("i: REP ILAS Checker\r\n");
	printf("c: DAC Check Status\r\n");
	printf("q: Quit Application \r\n");
	printf("******************** \r\n");

	while (quit_flag == 0)
	{
		int amp;
		int freq;
		int mode = 0;
		char_count  = read(fd, &buffer[0], 2);
		if (char_count >0 )  {
			switch(buffer[0]) {
			case 'q':
				printf("q: Quiting and Shutting Down\r\n");
				quit_flag = 1;
				break;
			case 'j':
				printf("j: Configuring JESD \r\n");
				printf("****Select JESD MODE ******* \r\n");
				printf("Select JESD Mode:");
				scanf("%d", &mode);
				printf("Setting JESD  Mode %d \r\n", mode);
				APP_set_jesd_mode(mode);
				APP_main();
				break;
			case 'e':
				printf("e: Enabling Mode \r\n");
				printf("Ensure Data Generator is Ready\r\n");
				APP_enable_mode();
				APP_main();
				break;
			case 's':
				printf("s: Checking Mode Status\r\n");
				APP_check_mode();
				break;
			case 'm':
				printf("m: Monitoring Interrupts \r\n");
				currAppMode =1;
				break;
			case 't':
				printf("n: Configuring DC Test Mode (NCO Only) \r\n");
				printf("**** Set Desired Carrier Freq ******* \r\n");
				printf("Enter desired carrier Frequency in MHz: ");
				scanf("%d", &freq);
				printf("**** Set Desired Amplitude  ******* \r\n");
				printf("Enter desired Amplitude for DC Test Mode: ");
				scanf("%d", &amp);	
				APP_set_nco_only_mode((uint64_t)freq, (uint16_t)amp);
				break;
			case 'n':
				printf("n: Configuring  NCO Shift\r\n");
				printf("**** Set NCO Shift ******* \r\n");
				printf("Enter desired NCO Shift MHz: ");
				scanf("%d", &freq);
				printf("Setting NCO for:  %d MHz Shift \r\n", freq);
				APP_set_nco_shift((uint64_t)freq);
				break;
			case 'r':
				printf("r: Reset to Default \r\n");
				APP_reset();
				break;
			case 'p':
				printf("p: JESD204B PRBS Checker\r\n");
				uint8_t result = 0;
				while(result == 0){
					delay_us(1000*10);
					result = APP_PRBS_checker();
				}
				printf("PHYSICAL LAYER IS OKAY!\r\n");
				printf("############ SUCCESS! ############\r\n");
				break;
			case 'i':
				printf("i: REP ILAS Checker\r\n");
				APP_enable_mode_repeated_ILAS();
				break;
			case 'c':
				printf("c: DAC Check Status\r\n");
				APP_CheckLink();
				break;
			case '\n':
				break;
			default:
				printf("Error: Unknown cmd \r\n");
				break;
			}
		}else {
			if(currAppMode !=0) {
				APP_main();
			}
		}

	}

	/*Shutdown Application*/
	currAppMode = 0;
	APP_shutdown();

	return 0;
}
