
// C library headers
#include <stdio.h>
#include <string.h>

// Linux headers
#include <fcntl.h> // Contains file controls like O_RDWR
#include <errno.h> // Error integer and strerror() function
#include <termios.h> // Contains POSIX terminal control definitions
#include <unistd.h> // write(), read(), close()



#include <stdint.h>





	
void spi_init()
{
    int serial_port = open("/dev/ttyUSB0", O_RDWR);

    // Check for errors
    if (serial_port < 0) {
        printf("Error %i from open: %s\n", errno, strerror(errno));
        printf("Error Opening UART to SPI connection failed! \n");
    }
}

int spi_open(int bus, int channel)
{
    return 0;
}

int spi_close()
{
    return 0;
}

int spi_set_mode(uint8_t m)
{
    return 0;
}

int spi_set_clk_polarity(uint8_t pol)
{

    return 0;
}

int spi_set_clk_phase(uint8_t phase)
{

    return 0;
}

int spi_set_lsb_first(int lsb_first)
{

    return 0;
}

int spi_set_bits_per_word(int bits)
{

    return 0;
}

int spi_set_speed(uint32_t speed)
{

    return 0;
}


int spi_xfer_ad916x(void *user_data, uint8_t *wbuf, uint8_t *rbuf, int len)
{

    return 0;
}
int spi_xfer_ad9508(void *user_data, uint8_t *wbuf, uint8_t *rbuf, int len)
{

    return 0;
}

int spi_xfer_adf4355(void *user_data, uint8_t *wbuf, uint8_t *rbuf, int len)
{

    return 0;
}
int spi_xfer_ad916x_v(void *user_data, uint8_t *wbuf, uint8_t *rbuf, int len)
{

    return 0;
}
int spi_xfer_ad9508_v(void *user_data, uint8_t *wbuf, uint8_t *rbuf, int len)
{

    return 0;
}
int spi_xfer_adf4355_v(void *user_data, uint8_t *wbuf, uint8_t *rbuf, int len)
{

    return 0;
}
void spi_configure(void)
{


}

int delay_us(unsigned int time_us)
{
    return 0;
}

#ifdef __cplusplus
}
#endif


