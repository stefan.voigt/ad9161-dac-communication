
// C library headers
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

// Linux headers
#include <fcntl.h> // Contains file controls like O_RDWR
#include <errno.h> // Error integer and strerror() function
#include <termios.h> // Contains POSIX terminal control definitions
#include <unistd.h> // write(), read(), close()
#include <time.h>

// UART message format
#include "uartMsgFormat.h"

#define TTY_DEVICE  "/dev/ttyUSB0"
#define BAUDRATE    B115200
#define EOL_CHAR    '\n' // Used on the FPGA side to determine the end of an input

//#define SPECIAL_CHAR_INTERPRETATION

int uart; // our serial input/output device
char read_buf [256];

void dataCheck(union SPIMessage msg){
    for(int i=0;i<sizeof(msg.bytes);i++){
        if(msg.bytes[i] == EOL_CHAR){
            printf("ERROR! THE SPI MESSAGE CONTAINS THE EOL CHAR!\r\n");
            exit(0);
        }
    }
}

void uartTxSpiMsg(union SPIMessage msg){
    dataCheck(msg);
    write(uart,msg.bytes,sizeof(msg));
    uint8_t eol = EOL_CHAR;
    write(uart,&eol,1);
}

int uartRx(void){
    int n = read(uart, &read_buf, sizeof(read_buf));
    return (n<=0) ? 0 : n;
}

void init_Uart(uint32_t baudrate) {
    printf("INIT UART\r\n");
    uart = open(TTY_DEVICE, O_RDWR);

    if (uart < 0) {
        printf("Error %i from open: %s\n", errno, strerror(errno));
        printf("Error Opening UART to SPI connection failed! \n");
        exit(0);
    }

    // configure the interface
    struct termios tty;
    if(tcgetattr(uart, &tty) != 0) {
        printf("Error %i from tcgetattr: %s\n", errno, strerror(errno));
    }
    tty.c_cflag &= ~PARENB; // Clear parity bit
    tty.c_cflag &= ~CSTOPB; // Clear stop field, only one stop bit used
    tty.c_cflag |= CS8; // 8 bits per byte
    tty.c_cflag &= ~CRTSCTS; // Disable RTS/CTS hardware flow control
    tty.c_cflag |= CREAD | CLOCAL; // Ignoring control lines & enable read
    tty.c_lflag &= ~ICANON; // Disable canonical mode
    tty.c_lflag &= ~ECHO; // Disable echo
    tty.c_lflag &= ~ECHOE; // Disable erasure
    tty.c_lflag &= ~ECHONL; // Disable new-line echo
    tty.c_lflag &= ~ISIG; // Disable interpretation of INTR, QUIT and SUSP
    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
    tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // Disable any special handling of received bytes
    #ifdef SPECIAL_CHAR_INTERPRETATION

    #else
        tty.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes (e.g. newline chars)
        tty.c_oflag &= ~ONLCR; // Prevent conversion of newline to carriage return/line feed
    #endif

    // Read configuration (blocking with timeout)
    tty.c_cc[VTIME] = 10;
    tty.c_cc[VMIN] = 0;


    // Baudrate
    cfsetispeed(&tty, baudrate);
    cfsetospeed(&tty, baudrate);

    // Save tty settings, also checking for error
    if (tcsetattr(uart, TCSANOW, &tty) != 0) {
        printf("Error %i from tcsetattr: %s\n", errno, strerror(errno));
    }

    // TX 'ALIVE' command
    union SPIMessage msg;
    msg.command = CMD_ALIVE;
    msg.slaveID = SLAVE_FPGA;
    msg.data = 0;

    // get a alive response from the FPGA
    int i=0;
    while(read_buf[0] != CMD_ALIVE){
        if(i>0){
            printf("Retries to connect to FPGA: %i\r\n",i++);
            delay_us(1000*1000);
        }
        uartTxSpiMsg(msg);
        uartRx();
    }
    printf("== GOT AN ALIVE BACK FROM THE FPGA! ==\r\n");
}
	
void spi_init()
{
    init_Uart(BAUDRATE);
}

int spi_open(int bus, int channel)
{
    return 0;
}

int spi_close()
{
    close(uart);
    return 0;
}

int spi_set_mode(uint8_t m)
{
    return 0;
}

int spi_set_clk_polarity(uint8_t pol)
{

    return 0;
}

int spi_set_clk_phase(uint8_t phase)
{

    return 0;
}

int spi_set_lsb_first(int lsb_first)
{

    return 0;
}

int spi_set_bits_per_word(int bits)
{

    return 0;
}

int spi_set_speed(uint32_t speed)
{

    return 0;
}


int spi_xfer_ad916x(void *user_data, uint8_t *wbuf, uint8_t *rbuf, int len)
{
    union SPIMessage msg;
    // The ad9161 can tx and rx on our board.
    // Therefore we need a read operation for xfers that allow readback
    msg.command = CMD_READ;
    msg.slaveID = SLAVE_AD9161;
	
	if (len != 3)
	{
		return -1;
	}
    
    uint16_t regval;
	uint16_t address;

	address = wbuf[0];
	address <<= 8;
	address |= wbuf[1];

	regval = wbuf[2];
	regval <<= 8;
	regval |= 0x00;//wbuf[2];

    uint32_t dataToTx = address;
    dataToTx <<= 16;
    dataToTx |=regval;

    msg.data = dataToTx;
    uartTxSpiMsg(msg);
    int n = uartRx();
    if(n > 0){
        // output the received data to the rbuffer
        rbuf[2] = read_buf[0];
    }


    return 0;
}
int spi_xfer_ad9508(void *user_data, uint8_t *wbuf, uint8_t *rbuf, int len)
{
    union SPIMessage msg;
    msg.command = CMD_WRITE;
    msg.slaveID = SLAVE_AD9508;
	
	if (len != 3)
	{
		return -1;
	}
    
    uint16_t regval;
	uint16_t address;

	address = wbuf[0];
	address <<= 8;
	address |= wbuf[1];

	regval = wbuf[2];
	regval <<= 8;
	regval |= 0x00;//wbuf[2];

    uint32_t dataToTx = address;
    dataToTx <<= 16;
    dataToTx |=regval;

    msg.data = dataToTx;
    uartTxSpiMsg(msg);

    return 0;
}

int spi_xfer_adf4355(void *user_data, uint8_t *wbuf, uint8_t *rbuf, int len)
{
    union SPIMessage msg;
    msg.command = CMD_WRITE;
    msg.slaveID = SLAVE_ADF4355;
	uint16_t regval;
	uint16_t address;
	
	if (len != 4)
	{
		//2 bytes for address and 2 byte data
		return -1;
	}

    

	address = wbuf[0];
	address <<= 8;
	address |= wbuf[1];

	regval = wbuf[2];
	regval <<= 8;
	regval |= wbuf[3];

    uint32_t dataToTx = address;
    dataToTx <<= 16;
    dataToTx |=regval;

    msg.data = dataToTx;
    uartTxSpiMsg(msg);

    return 0;
}
int spi_xfer_ad916x_v(void *user_data, uint8_t *wbuf, uint8_t *rbuf, int len)
{
    printf("################\n");
    return 0;
}
int spi_xfer_ad9508_v(void *user_data, uint8_t *wbuf, uint8_t *rbuf, int len)
{
    printf("################\n");
    return 0;
}
int spi_xfer_adf4355_v(void *user_data, uint8_t *wbuf, uint8_t *rbuf, int len)
{
    printf("################\n");
    return 0;
}
void spi_configure(void)
{


}

int delay_us(unsigned int time_us)
{
    uint32_t retval = 0;
    struct timespec t0;
    struct timespec t1;
    struct timespec *tmp;
    struct timespec *waitTime = &t0;
    struct timespec *remainTime = &t1;

    waitTime->tv_sec  = time_us/1000000;
    waitTime->tv_nsec = (time_us % 1000000) * (1000);

    do {
        retval = nanosleep(waitTime, remainTime);
        tmp = waitTime;
        waitTime = remainTime;
        remainTime = tmp;
    }while((retval == -1) && (errno == EINTR));

    if(retval)
    {
        return errno;
    }
    else
    {
        return 0;
    }
}

#ifdef __cplusplus
}
#endif


