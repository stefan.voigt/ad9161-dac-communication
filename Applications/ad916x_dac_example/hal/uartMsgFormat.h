#ifndef __UART_MSG_FORMAT_H__
#define __UART_MSG_FORMAT_H__
#include <stdint.h>

/* UART MSG FORMAT: || CMD || SLAVE ID || DATA ||
*                     8 Bit    8 Bit     32 Bit  (fix)
* The DATA field contains both, the address and the user data for the SPI to write.
* The reason for a fixed packet size is a much easier implementation on the FPGA.
*/

#pragma pack(push,1)
union SPIMessage
{
    struct{
        uint8_t  command;
        uint8_t  slaveID;
        uint32_t data;
    };
    uint8_t  bytes[2*sizeof(uint8_t)+sizeof(uint32_t)];
};
#pragma pack(pop)


// COMMANDS
#define CMD_READ    'R'
#define CMD_WRITE   'W'
#define CMD_ALIVE   'A' // used to test the communication interface to the FPGA

// SLAVES
#define SLAVE_FPGA      0
#define SLAVE_AD9161    1
#define SLAVE_AD9508    2
#define SLAVE_ADF4355   3

#endif /* !__UART_MSG_FORMAT_H__*/
