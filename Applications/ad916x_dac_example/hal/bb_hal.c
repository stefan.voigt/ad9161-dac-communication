#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <linux/spi/spidev.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include "hal.h"

#define FPGA_SPI_REG_100 0x100

#define SPIM_RBUF_RDDATA 0x201
#define SPIM_CTL 0x220
#define SPIM_XFER_START 0x0221
#define SPIM_XFER_COUNT 0x0222
#define SPIM_CS_ENABLE 0x0224
#define SPIM_CS_POLARITY 0x0225
#define SPIM_CS_COUNT 0x0226
#define SPIM_WR_DELAY_COUNT 0x0227
#define SPIM_WR_BYTE_COUNT 0x0228
#define SPIM_WBUF_WRDATA 0x0229
#define SPIM_RD_DELAY_COUNT 0x022a
#define SPIM_RD_BYTE_COUNT 0x022b
#define SPIM_SCRATCH 0x02ff

#define CS_AD916X (1u << 0)
#define CS_AD9508 (1u << 1)
#define CS_ADF4355 (1u << 2)

#define MAX_PATH_LEN  40
#define SPI_DEVICE_PATH_BASE "/dev/spidev"
#define LS_SPI_DEVICE_PATH_BASE "ls " SPI_DEVICE_PATH_BASE "*"
#define SPI_DEVICE_MOUNT "cd /lib/firmware && echo BB-SPIDEV0 > /sys/devices/bone_capemgr.9/slots"

#define SHELLSCRIPT "\
#/bin/bash \n\
spi =$ (cat / sys / devices / bone_capemgr.9 / slots | sed - n '/BB-SPIDEV0/p') \n\
if[-z \"$spi\" - a \"$spi\" != \" \"]; then \n\
        cd /lib/firmware \n\
        echo BB - SPIDEV0 > /sys/devices/bone_capemgr.9/slots \n\
fi \n\
"
//system(SHELLSCRIPT);
	
static int active = 0;
static uint8_t mode = 0;
static uint8_t bpw = 0;
static uint32_t speed = 0;
static int fd = -1;
static int lsb_first = 0;
static char buff[MAX_PATH_LEN];
static char path[MAX_PATH_LEN];

static int exec_shell(const char *cmd, char *buff, int size)
{
	FILE *fp;

	/* Open the command for reading. */
	fp = popen(cmd, "r");
	if (fp == NULL)
	{
	/*	printf("Failed to run command\n"); */
		return 1;
	}

	/* Read the output a line at a time - output it. */
	fgets(buff, size - 1, fp);

	/* close */
	pclose(fp);
	return 0;
}

void spi_init()
{
	active = 0;
	mode = 0;
	bpw = 0;
	speed = 0;
	fd = -1;
	lsb_first = 0;
}

int spi_open(int bus, int channel)
{
	int r;
	uint32_t tmp32;
	uint8_t tmp;
	char *pch;

	if (active != 0)
	{
		close(fd);	
	}
	else
	{
		if (exec_shell(LS_SPI_DEVICE_PATH_BASE, buff, sizeof(buff)) != 0)
		{
			return -ENODEV;
		}
		
		pch = strpbrk(buff, SPI_DEVICE_PATH_BASE);
		
		if (pch == 0)
		{
			if (exec_shell(SPI_DEVICE_MOUNT, buff, sizeof(buff)) != 0)
			{
				return -ENODEV;
			}
			if (exec_shell(LS_SPI_DEVICE_PATH_BASE, buff, sizeof(buff)) != 0)
			{
				return -ENODEV;
			}
		
			pch = strpbrk(buff, SPI_DEVICE_PATH_BASE);
			if (pch == 0)
			{
				return -ENODEV;
			}			
		}
	}

	if (bus < 0 || channel < 0)
	{
		return -ENODEV;
	}

	if (snprintf(path, MAX_PATH_LEN, "%s%d.%d", SPI_DEVICE_PATH_BASE, bus, channel) >= MAX_PATH_LEN)
	{
		return -EINVAL;	
	}

	if ((fd = open(path, O_RDWR, 0)) < 0)
	{
		printf("open(%s) failed\n", path);
		return fd;
	}

	if ((r = ioctl(fd, SPI_IOC_RD_MODE, &tmp)) < 0)
	{
		printf("ioctl(fd, SPI_IOC_RD_MODE, &tmp) failed\n");
		return r;
	}
	mode = tmp;

	if ((r = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &tmp)) < 0)
	{
		printf("ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &tmp) failed\n");
		return r;
	}
	bpw = tmp;

	if ((r = ioctl(fd, SPI_IOC_RD_LSB_FIRST, &tmp)) < 0)
	{
		printf("ioctl(fd, SPI_IOC_WR_LSB_FIRST, &tmp) failed\n");
		return r;
	}
	lsb_first = lsb_first;

	if ((r = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &tmp32)) < 0)
	{
		printf("ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &tmp) failed\n");
		return r;
	}
	speed = tmp32;
	active = 1;
	
	return 0;
}

int spi_close()
{
	int tmpfd = fd;
	if (!active)
	{
		return -ENODEV;
	}
	
	mode = 0;
	bpw = 0;
	speed = 0;
	active = 0;
	fd = -1;
	return close(tmpfd);
}

int spi_set_mode(uint8_t m)
{
	m &= SPI_CPHA | SPI_CPOL;
	m = (mode & ~(SPI_CPHA | SPI_CPOL)) | m;

	int r = ioctl(fd, SPI_IOC_WR_MODE, &m);
	if (r < 0)
	{
		return r;
	}

	r = ioctl(fd, SPI_IOC_RD_MODE, &m);
	if (r < 0)
	{
		return r;
	}

	mode = m;

	return 0;
}

int spi_set_clk_polarity(uint8_t pol)
{
	pol &= SPI_CPOL;
	uint8_t mode = (mode & ~(SPI_CPOL)) | pol;
	return spi_set_mode(mode);
}

int spi_set_clk_phase(uint8_t phase)
{
	phase &= SPI_CPHA;
	uint8_t mode = (mode & ~(SPI_CPHA)) | phase;
	return spi_set_mode(mode);
}

int spi_set_lsb_first(int lsb_first)
{
	int r;
	if (!active)
	{
		return -ENODEV;
	}
	if ((r = ioctl(fd, SPI_IOC_WR_LSB_FIRST, &lsb_first)) < 0)
	{
		return r;
	}
	lsb_first = lsb_first;
	return 0;
}

int spi_set_bits_per_word(int bits)
{
	int r;
	if (!active)
	{
		return -ENODEV;
	}

	if ((r = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits)) < 0)
	{
		return r;
	}

	bpw = bits;
	return 0;
}

/* CPHA - clock phase 
   CPOL - clock polarity
*/
int spi_set_speed(uint32_t speed)
{
	int r;
	uint32_t tmp;

	if (!active)
	{
		return -ENODEV;
	}

	r = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (r < 0)
	{
		printf("ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed): %s", strerror(r));
		return r;
	}

	r = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &tmp);
	if (r < 0)
	{
		printf("ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed): %s", strerror(r));
		return r;
	}
	speed = tmp;
	return 0;
}

int spi_xfer1(uint8_t *wbuf, uint8_t *rbuf, int len)
{
	struct spi_ioc_transfer txinfo;
	txinfo.tx_buf = (__u64) wbuf;
	txinfo.rx_buf = (__u64) rbuf;
	txinfo.len = len;
	txinfo.delay_usecs = 0;
	txinfo.speed_hz = speed;
	txinfo.bits_per_word = bpw;
	txinfo.cs_change = 1;

	int r = ioctl(fd, SPI_IOC_MESSAGE(1), &txinfo);
	if (r < 0)
	{
		printf("ioctl(fd, SPI_IOC_MESSAGE(1), &txinfo): %s (len=%d)\n",
			strerror(r),
			len);
		return r;
	}

	//0 - OK
	return 0;
}

void spi_write_master(uint16_t address, uint16_t data)
{
	uint8_t rd[4];
	uint8_t wr[4];
	//
	wr[3] = data & 0xFF;
	wr[2] = (data >> 8) & 0xFF;
	wr[1] = address & 0xFF;
	wr[0] = (address >> 8) & 0xFF;
	spi_xfer1(wr, rd, sizeof(rd));
}

void spi_read_master(uint16_t address, uint8_t *data)
{
	uint8_t rd[4];
	uint8_t wr[4];
	//
	wr[3] = 0xFF;
	wr[2] = 0xFF;
	wr[1] = address & 0xFF;
	wr[0] = (address >> 8) & 0xFF;
	wr[0] |= 0x80;
	spi_xfer1(wr, wr, sizeof(wr));
	*data = wr[2];
}

void fpga_read(uint16_t address, uint16_t *data)
{
	uint8_t rd[4];
	uint8_t wr[4];
	//
	wr[3] = 0xFF;
	wr[2] = 0xFF;
	wr[1] = address & 0xFF;
	wr[0] = (address >> 8) & 0xFF;
	wr[0] |= 0x80;
	spi_xfer1(wr, wr, sizeof(wr));
	*data = wr[2];
	*data <<= 8;
	*data |= wr[3];
}

void spi_configure(void)
{
	uint16_t data;
	fpga_read(FPGA_SPI_REG_100, &data);
	data |= 0x0008;
	spi_write_master(FPGA_SPI_REG_100, data);
	data &= ~0x0008;
	spi_write_master(FPGA_SPI_REG_100, data);
	spi_write_master(SPIM_CTL, 0);
	spi_write_master(SPIM_CTL, 0x0140);
	spi_write_master(SPIM_XFER_START, 0);
	spi_write_master(SPIM_CS_COUNT, 24);
	spi_write_master(SPIM_CS_POLARITY, 0);
	spi_write_master(SPIM_RD_BYTE_COUNT, 1); //read one byte
	spi_write_master(SPIM_RD_DELAY_COUNT, 16); //16bit (2 byte) address skipped
	spi_write_master(SPIM_WR_BYTE_COUNT, 3); //write 2 byte address
	spi_write_master(SPIM_WR_DELAY_COUNT, 0); //...first thing
	spi_write_master(SPIM_XFER_COUNT, 1);
}

int spi_xfer_ad916x(void *user_data, uint8_t *wbuf, uint8_t *rbuf, int len)
{
	uint16_t address;
	uint16_t value;
	
	if (len != 3)
	{
		//2 bytes for adderss and 1 byte data
		return -1;
	}

	address = wbuf[0];
	address <<= 8;
	address |= wbuf[1];
	value = wbuf[2];
	value <<= 8;
	value |= 0xFF;

	spi_write_master(SPIM_CS_ENABLE, CS_AD916X);
	spi_write_master(SPIM_CS_COUNT, 24);
	spi_write_master(SPIM_WR_BYTE_COUNT, 3);
	spi_write_master(SPIM_RD_BYTE_COUNT, 1);
	spi_write_master(SPIM_WBUF_WRDATA, address);
	spi_write_master(SPIM_WBUF_WRDATA, value);
	spi_write_master(SPIM_XFER_START, 1);
	spi_write_master(SPIM_XFER_START, 0);

	spi_read_master(SPIM_RBUF_RDDATA, &rbuf[2]);
	return 0;
}

int spi_xfer_ad9508(void *user_data, uint8_t *wbuf, uint8_t *rbuf, int len)
{
	uint16_t address;
	uint16_t value;
	
	if (len != 3)
	{
		//2 bytes for adderss and 1 byte data
		return -1;
	}

	address = wbuf[0];
	address <<= 8;
	address |= wbuf[1];
	value = wbuf[2];
	value <<= 8;
	value |= 0xFF;

	spi_write_master(SPIM_CS_ENABLE, CS_AD9508);
	spi_write_master(SPIM_CS_COUNT, 24);
	spi_write_master(SPIM_WR_BYTE_COUNT, 3);
	spi_write_master(SPIM_RD_BYTE_COUNT, 1);
	spi_write_master(SPIM_WBUF_WRDATA, address);
	spi_write_master(SPIM_WBUF_WRDATA, value);
	spi_write_master(SPIM_XFER_START, 1);
	spi_write_master(SPIM_XFER_START, 0);

	spi_read_master(SPIM_RBUF_RDDATA, &rbuf[2]);
	return 0;
}

int spi_xfer_adf4355(void *user_data, uint8_t *indata, uint8_t *outdata, int size_bytes)
{
	uint16_t regval;
	uint16_t address;
	
	if (size_bytes != 4)
	{
		//2 bytes for adderss and 2 byte data
		return -1;
	}

	address = indata[0];
	address <<= 8;
	address |= indata[1];

	regval = indata[2];
	regval <<= 8;
	regval |= indata[3];

	spi_write_master(SPIM_CS_ENABLE, CS_ADF4355);
	spi_write_master(SPIM_CS_COUNT, 32);
	spi_write_master(SPIM_WR_BYTE_COUNT, 4);
	spi_write_master(SPIM_RD_BYTE_COUNT, 0);
	spi_write_master(SPIM_WBUF_WRDATA, address);
	spi_write_master(SPIM_WBUF_WRDATA, regval);
	spi_write_master(SPIM_XFER_START, 1);
	spi_write_master(SPIM_XFER_START, 0);

	return 0;
}

#define AD9508_NUM_REGS_ 45
#define AD916X_NUM_REGS_ 0x7FFF
#define ADF4355_NUM_REGS_ 13
uint32_t adf4355_registers[ADF4355_NUM_REGS_] = {0};
uint32_t ad9508_registers[AD9508_NUM_REGS_] = {0};
uint32_t ad916x_registers[AD916X_NUM_REGS_] = {0};

int spi_xfer_ad916x_v(void *user_data, uint8_t *indata, uint8_t *outdata, int size_bytes)
{
	uint16_t address;
	uint8_t value;
	
	if (size_bytes != 3)
	{
		/* 2 bytes for adderss and 1 byte data */
		return 2;
	}
	
	address = indata[0];
	address <<= 8;
	address |= indata[1];
	value = indata[2];
	
	if ((address & 0x8000) == 0)
	{
		/* Write */
		ad916x_registers[address] = value;
		outdata[2] = 0xFF;
	}
	else
	{
		/* Read */
		/* Clear the read bit as we read from local array */
		address &= ~0x8000;
	
		if (address > AD916X_NUM_REGS_)
		{
			/* Invalid address */
			return 1;
		}
		outdata[2] = ad916x_registers[address];
	}
	return 0;
}

int spi_xfer_ad9508_v(void *user_data, uint8_t *indata, uint8_t *outdata, int size_bytes)
{
	uint16_t address;
	uint8_t value;
	
	if (size_bytes != 3)
	{
		/* 2 bytes for adderss and 1 byte data */
		return 2;
	}
	
	address = indata[0];
	address <<= 8;
	address |= indata[1];
	value = indata[2];

	if ((address & 0x6000) != 0)
	{
		int nrbytes = (address >> 14) & 0x3;
		/* TODO: check nr bytes */
		address &= ~0x6000;
	}

	if ((address & 0x8000) == 0)
	{
		/* Write */
		ad9508_registers[address] = value;
		outdata[2] = 0xFF;
	}
	else
	{
		/* Read */
		/* Clear the read bit as we read from local array */
		address &= ~0x8000;
	
		if (address > AD9508_NUM_REGS_)
		{
			/* Invalid address */
			return 1;
		}
		outdata[2] = ad9508_registers[address];
	}
	return 0;
}

int spi_xfer_adf4355_v(void *user_data, uint8_t *indata, uint8_t *outdata, int size_bytes)
{
	uint32_t regval;
	uint8_t address;
	
	if (size_bytes != 4)
	{
		return 2;
	}
	
	address = indata[3] & 0x0F;
	regval = indata[0];
	regval <<= 8;
	regval |= indata[1];
	regval <<= 8;
	regval |= indata[2];
	regval <<= 8;
	regval |= indata[3];
	
	if (address > ADF4355_NUM_REGS_)
	{
		/* Invalid address */
		return 1;
	}

	/* Write */
	adf4355_registers[address] = regval;
	outdata[2] = 0xFF;
	return 0;
}

int delay_us(unsigned int time_us)
{
    uint32_t retval = 0;
    struct timespec t0;
    struct timespec t1;
    struct timespec *tmp;
    struct timespec *waitTime = &t0;
    struct timespec *remainTime = &t1;

    waitTime->tv_sec  = time_us/1000000;
    waitTime->tv_nsec = (time_us % 1000000) * (1000);

    do {
        retval = nanosleep(waitTime, remainTime);
        tmp = waitTime;
        waitTime = remainTime;
        remainTime = tmp;
    }while((retval == -1) && (errno == EINTR));

    if(retval)
    {
        return errno;
    }
    else
    {
        return 0;
    }

}
