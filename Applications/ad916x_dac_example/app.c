/**
 * \brief Contains Application functions
 *
 */

#include <stdio.h>

#include "hal.h"
#include "app_config.h"
#include "app.h"
#include "app_error.h"
#include "app_dac_cfg.h"
#include "app_clk_cfg.h"

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0') 



/*Table 25*/ // NOT!

// Table 28: Interpolation Rate, No. of Lanes, M, F, S
#define NOF_JESD_MODES 16
int JESD_INT_MODES[] = {
    //1,8,2,1,4,

	16,2,2,2,1,

	2,6,2,2,3,
    2,8,2,1,2,
    3,6,2,2,3,
    3,8,2,1,2,
    4,6,2,2,3,
    4,8,2,1,2,
    6,6,2,2,3,
    6,8,2,1,2,
    8,6,2,2,3,
    8,8,2,1,2,
    12,6,2,2,3,
    12,8,2,1,2,
    16,6,2,2,3,
    16,8,2,1,2,
    24,6,2,2,3,
    24,8,2,1,2,
};

int APP_init(void)
{
	int appError = APP_ERR_OK;

	/*Initialise SPI Driver on Platform*/
	printf("APP:INIT: SPI \r\n");
	spi_init();
	spi_open(1,0);
	spi_set_mode(0);
	spi_set_speed(1e6);
	spi_configure();

#if ON_BOARD_CLK
	/*Initialise Clock Chip That Provides Reference Clock to DAC*/
	printf("APP:INIT: Clock \r\n");
	appError = clk_init();
	if(appError != APP_ERR_OK) {
		printf("APP:INIT: CLK init Failed \r\n");
	}
	// while(1){
	// 	clk_set_mode(EXT_REF_CLK_MHZ,DAC_RATE_MHZ, DEF_LANE_RATE_MHZ);
	// 	delay_us(10);
	// }
	appError = clk_set_mode(EXT_REF_CLK_MHZ,DAC_RATE_MHZ, DEF_LANE_RATE_MHZ);
	if(appError != APP_ERR_OK) {
		printf("APP:INIT: Clk set to %d failed\r\n", DAC_RATE_MHZ);
	}

	delay_us(1500);
#if DEBUG
	clk_check_config();
#endif

#endif

    /*Initialise DAC module  */
	printf("APP:INIT: DAC \r\n");
	appError = dac_init(&dac_h);
	if (appError != APP_ERR_OK) {
		printf("APP:INIT: DAC init Failed \r\n");
		exit(0);
		return appError;
	}
#if DEBUG
	appError = dac_check_init(&dac_h);
	if(appError != APP_ERR_OK) {
		printf("APP:INIT: DAC init Check Failed \r\n");
		return appError;
	}
#endif

	dac_shutdown(&dac_h);
	clk_shutdown();

	#if ON_BOARD_CLK
	/*Initialise Clock Chip That Provides Reference Clock to DAC*/
	printf("APP:INIT: Clock \r\n");
	appError = clk_init();
	if(appError != APP_ERR_OK) {
		printf("APP:INIT: CLK init Failed \r\n");
	}
	// while(1){
	// 	clk_set_mode(EXT_REF_CLK_MHZ,DAC_RATE_MHZ, DEF_LANE_RATE_MHZ);
	// 	delay_us(10);
	// }
	appError = clk_set_mode(EXT_REF_CLK_MHZ,DAC_RATE_MHZ, DEF_LANE_RATE_MHZ);
	if(appError != APP_ERR_OK) {
		printf("APP:INIT: Clk set to %d failed\r\n", DAC_RATE_MHZ);
	}

	delay_us(1500);
#if DEBUG
	clk_check_config();
#endif

#endif

    /*Initialise DAC module  */
	printf("APP:INIT: DAC \r\n");
	appError = dac_init(&dac_h);
	if (appError != APP_ERR_OK) {
		printf("APP:INIT: DAC init Failed \r\n");
		exit(0);
		return appError;
	}
#if DEBUG
	appError = dac_check_init(&dac_h);
	if(appError != APP_ERR_OK) {
		printf("APP:INIT: DAC init Check Failed \r\n");
		return appError;
	}
#endif

	return appError;
}
int APP_reset()
{
	int appError = APP_ERR_OK;

	/*Reset DAC module  */
	printf("APP:Reset : DAC \r\n");
	appError = dac_reset(&dac_h);
	if(appError != APP_ERR_OK) {
		printf("APP:Reset: DAC reset  Failed \r\n");
	}
	return appError;
}

int APP_shutdown(void)
{
	int appError = APP_ERR_OK;

	/*Shutdown DAC module  */
	printf("APP:SHTDWN: DAC \r\n");
	appError = dac_shutdown(&dac_h);
	if(appError != APP_ERR_OK) {
		printf("APP:SHTDWN: DAC shutdown Failed \r\n");
		return appError;
	}

#if ON_BOARD_CLK
	/*Shutdown Clock module  */
	printf("APP:SHTDWN: Clock \r\n");
	appError = clk_shutdown();
	if(appError != APP_ERR_OK) {
		printf("APP:SHTDWN: CLK  shutdown Failed \r\n");
		return appError;
	}
#endif

	printf("APP:SHTDWN: SPI \r\n");
	spi_close();

	return appError;
}

int APP_set_jesd_mode(int mode)
{
	int appError = APP_ERR_OK;
	int lane_rate = 0;
	int interpol = 1;
	int lanes = 8;
	dac_jesd_data jesd_data;

	printf("APP: Setting JESD Test Mode: %d \r\n", mode);
	if (mode > NOF_JESD_MODES) {
		printf("APP:ERROR:INVALID JESD MODE");
		return APP_ERR_MODE_INVALID;
	}

	/*calculate jesd lane Rate*/
	interpol = JESD_INT_MODES[(mode*5)];
	lanes = JESD_INT_MODES[(mode*5)+1];
	if(interpol == 1 ) {
		lane_rate = (((DAC_RATE_MHZ*20))/lanes);
	}
	else if (interpol > 1) {
		lane_rate = (((DAC_RATE_MHZ * 2*20)/interpol)/lanes);
	}
	else {
		return APP_ERR_MODE_INVALID;
	}
	printf("APP:INIT:DAC set Mode (JESD): Lane rate: %d\r\n", lane_rate);

	/*Update DAC Clock Appropriately*/
	appError = clk_set_mode(EXT_REF_CLK_MHZ,DAC_RATE_MHZ,lane_rate);

	/*Config DAC Mode to JESD Mode*/
	jesd_data.interpol = interpol;
	jesd_data.L = lanes;
	jesd_data.M = JESD_INT_MODES[(mode*5)+2];
	jesd_data.F = JESD_INT_MODES[(mode*5)+3];
	jesd_data.S = JESD_INT_MODES[(mode*5)+4];

	printf("################################\r\n");
	printf("APP:INIT: The selected Mode is: \r\n");
	printf("Interpolation: %i\r\n", interpol);
	printf("Lane count   : %i\r\n", lanes);
	printf("JESD204B M   : %i\r\n", jesd_data.M);
	printf("JESD204B F   : %i\r\n", jesd_data.F);
	printf("JESD204B S   : %i\r\n", jesd_data.S);
	printf("################################\r\n");

	appError = dac_set_mode(&dac_h, DAC_JESD_MODE, DAC_RATE_MHZ, &jesd_data);
	if(appError != APP_ERR_OK) {
		printf("APP:INIT: DAC set Mode (JESD) Failed \r\n");
	}

	return appError;
}

int APP_set_nco_only_mode(uint64_t carrier_freq_mhz, uint16_t amplitude)
{
	int appError = APP_ERR_OK;
	dac_nco_data nco_data;

	printf("APP: Setting NCO Only DC TEST  Mode: %d MHz @ %d \r\n",carrier_freq_mhz, amplitude);
	nco_data.carrier_freq = (((uint64_t)carrier_freq_mhz) *1000 *1000);
	nco_data.amplitude = amplitude;

	/*Update DAC Clock Appropriately*/
	appError = clk_set_mode(EXT_REF_CLK_MHZ,DAC_RATE_MHZ,5000);

	/*Config DAC Mode to DC Test Mode*/
	appError = dac_set_mode(&dac_h, DAC_NCO_ONLY_MODE, DAC_RATE_MHZ, &nco_data);

	return appError;
}

int APP_set_nco_shift(uint64_t carrier_freq_mhz)
{
	int appError = APP_ERR_OK;
	dac_nco_data nco_data;

	printf("APP: Setting NCO Mode: %d MHz \r\n",(int)carrier_freq_mhz);
	nco_data.carrier_freq = (((uint64_t)carrier_freq_mhz) *1000 *1000);
	nco_data.amplitude = 0;

	printf("APP: Setting NCO Mode: %d Hz \r\n",(int) nco_data.carrier_freq);
	/*Config NCO for Digital Modulation*/
	appError = dac_set_mode(&dac_h, DAC_NCO_SHIFT, DAC_RATE_MHZ, &nco_data);

	return appError;
}

int APP_enable_mode(void)
{
	int appError = APP_ERR_OK;

	/*Enable JESD Mode on DAC*/
	appError = dac_enable_mode(&dac_h);
	if(appError != APP_ERR_OK) {
		printf("APP:RUN: DAC enable Mode (JESD) failed \r\n");
	}

	return appError;
}

int APP_enable_mode_repeated_ILAS(void)
{
	int appError = APP_ERR_OK;

	/*Enable JESD Mode on DAC*/
	appError = dac_enable_mode_repeated_ILAS(&dac_h);
	if(appError != APP_ERR_OK) {
		printf("APP:RUN: DAC enable Mode (JESD) failed \r\n");
	}

	return appError;
}

int APP_PRBS_checker(void)
{
	uint8_t result = 0;
	uint32_t checkThreshold = 100;        // bits
	uint8_t lanesToCheck    = 0b00000011; // every bit position equals one lane. Lane0 selected
	printf("\r\n");
	printf("##################################\r\n");
	printf("Checker pattern       : PRBS7\r\n");
	printf("Checker threshold     : %i bits\r\n",checkThreshold);
	printf("Checker lanes to check: 0b"BYTE_TO_BINARY_PATTERN,BYTE_TO_BINARY(lanesToCheck));printf("\r\n");
	printf("\r\n");
	ad916x_prbs_test_t testResult;
	ad916x_jesd_phy_prbs_test(&dac_h, PRBS7, lanesToCheck, checkThreshold, &testResult);
	printf("Result: PRBS Test Error Count       : %i\r\n",testResult.phy_prbs_err_cnt);
	printf("Result: PRBS Test Status            : 0b"BYTE_TO_BINARY_PATTERN,BYTE_TO_BINARY(testResult.phy_prbs_pass));
	(testResult.phy_prbs_pass == 0xff) ? printf("  ## SUCCESS ##\r\n") : printf("  ## FAILED ##\r\n");
	printf("Result: PRBS Test Source Error Count: %i\r\n",testResult.phy_src_err_cnt);
	printf("##################################\r\n");
	printf("\r\n");
	// delay_us(1000*1000); // 1sec delay
	//APP_PRBS_checker();
	return (testResult.phy_prbs_pass == 0xff);
}

int APP_CheckLink(void)
{
	int dacError = 0;
	ad916x_jesd_link_stat_t app_dac_link_status;
	dacError = ad916x_jesd_get_link_status(&dac_h, &app_dac_link_status);
	if (dacError != 0) {
		printf("DAC:MODE:JESD: ERROR : Get Link status failed \r\n");
		return APP_ERR_DAC_FAIL;
	}
	printf("\r\n");
	printf("code_grp_sync      : 0b"BYTE_TO_BINARY_PATTERN,BYTE_TO_BINARY(app_dac_link_status.code_grp_sync_stat));printf("\r\n");
	printf("frame_sync_stat    : 0b"BYTE_TO_BINARY_PATTERN,BYTE_TO_BINARY(app_dac_link_status.frame_sync_stat));printf("\r\n");
	printf("good_checksum_stat : 0b"BYTE_TO_BINARY_PATTERN,BYTE_TO_BINARY(app_dac_link_status.good_checksum_stat));printf("\r\n");
	printf("init_lane_sync_stat: 0b"BYTE_TO_BINARY_PATTERN,BYTE_TO_BINARY(app_dac_link_status.init_lane_sync_stat));printf("\r\n");
	// if(app_dac_link_status.code_grp_sync_stat != 0)
	// {
	// 	return app_dac_link_status.code_grp_sync_stat;
	// }
	delay_us(1000*50);
	APP_CheckLink();
}


int APP_check_mode(void)
{
	int appError = APP_ERR_OK;

	appError = dac_check_nco_config(&dac_h);
	if(appError != APP_ERR_OK) {
		printf("APP:Check JESD config failed\r\n");
	}

	return appError;
}

int APP_main(void)
{
	int appError = APP_ERR_OK;
	printf("APP: Main: \r\n");

	/*Monitor DAC*/
	appError = dac_monitor(&dac_h);
	if(appError != APP_ERR_OK) {
		printf("APP: Main: DAC Monitoring failed \r\n");
	}
	return appError;
}











