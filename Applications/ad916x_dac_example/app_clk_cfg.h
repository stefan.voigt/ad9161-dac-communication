/**
 * \file app_clk_cfg.h
 *
 * \brief Defines the Application - Clock  Module Interface
 *
 */

#ifndef APP_CLK_CFG_H_
#define APP_CLK_CFG_H_



int clk_init(void);
int clk_shutdown(void);
int clk_set_mode(int ref_clk_mhz, int dac_rate_mhz, int lane_rate_mhz);
int clk_check_config(void);
#endif
