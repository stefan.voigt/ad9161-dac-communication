#ifndef __AD9508_H__
#define __AD9508_H__

#include "api_def.h"
#include "api_config.h"
#include <stdint.h>


typedef struct
{
	void *user_data;
	spi_sdo_config_t sdo;
	spi_xfer_t dev_xfer;
	delay_us_t delay_us;
}ad9508_handle_t;


/**
 * \brief Initialize the API.
 *
 * This API must be called first before any other API calls.
 * It performs internal API initialization of the memory and API states.
 *
 * \param h pointer to the AD9508 device reference handle.
 *
 * \retval API_ERROR_OK API Completed Successfully
 * \retval API_ERROR_INVALID_PARAM    Invalid Parameter
 */
ADI_API int ad9508_init(ad9508_handle_t *h);
/**
 * \brief Reset the AD9508
 *
 *
 * \param h pointer to the AD9508 device reference handle.
 *
 * \retval API_ERROR_OK API Completed Successfully
 * \retval API_ERROR_INVALID_PARAM    Invalid Parameter
 */
ADI_API int ad9508_reset(ad9508_handle_t *h);

/**
 * \brief Perform SPI register write to the AD9508
 *
 * This API must be called first before any other API calls.
 * It performs internal API initialization of the memory and API states.
 *
 * \param h pointer to the AD9508 device reference handle.
 * \param Address 	Uint16 value representing the register address to
 * 					which the value of the data parameter shall be written.
 * \param data 		uint8_t value represent the data that shall be written to
 * 					the desired spi address.
 *
 * \retval API_ERROR_OK API Completed Successfully
 * \retval API_ERROR_INVALID_PARAM    Invalid Parameter
 */
ADI_API int ad9508_register_write(ad9508_handle_t *h, uint16_t address, uint8_t data);
#endif /* __AD9508_H__ */
