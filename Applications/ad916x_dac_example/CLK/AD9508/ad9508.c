#include "api_config.h"
#include <stdint.h>
#include "ad9508.h"
#include "api_errors.h"

#define AD9508_MAX_NUM_CHAN 4

#define AD9508_REG_SPI_CTL 0x0000
#define AD9508_REG_SILICON_REV 0x000A
#define AD9508_REG_PART_ID_L 0x000C
#define AD9508_REG_PART_ID_H 0x000D

#define AD9508_SPI_CTL_SDO_EN 0x81
#define AD9508_SPI_CTL_SOFT_RESET 0x24

#define AD9508_REG_SLEEP 0x0013
#define AD9508_REG_SYNC_BAR 0x0014

#define AD9508_OUT_CHAN_NUM_REGS 6
#define AD9508_REG_OUT_DIVIDE_RATIO_L(x) (0x0015 + ((x)*AD9508_OUT_CHAN_NUM_REGS))
#define AD9508_REG_OUT_DIVIDE_RATIO_H(x) (0x0016 + ((x)*AD9508_OUT_CHAN_NUM_REGS))
#define AD9508_REG_OUT_PHASE_L(x)        (0x0017 + ((x)*AD9508_OUT_CHAN_NUM_REGS))
#define AD9508_REG_OUT_PHASE_H(x)        (0x0018 + ((x)*AD9508_OUT_CHAN_NUM_REGS))
#define AD9508_REG_OUT_DRIVER(x)         (0x0019 + ((x)*AD9508_OUT_CHAN_NUM_REGS))
#define AD9508_REG_OUT_CMOS(x)           (0x001A + ((x)*AD9508_OUT_CHAN_NUM_REGS))

#define AD9508_SPI_READ 0x8000u
#define AD9508_SPI_1BYTE 0x0000u
#define AD9508_SPI_2BYTE 0x2000u
#define AD9508_SPI_3BYTE 0x4000u
#define AD9508_SPI_STREAM 0x6000u


#define AD9508_IN_OUT_BUFF_SZ 3



ADI_API int ad9508_register_write(ad9508_handle_t *h, uint16_t address, uint8_t data)
{
	int err = API_ERROR_INVALID_HANDLE_PTR;
	uint8_t inData[AD9508_IN_OUT_BUFF_SZ];
	uint8_t outData[AD9508_IN_OUT_BUFF_SZ];
		if (h != INVALID_POINTER)
	{
		if (h->dev_xfer == INVALID_POINTER)
		{
			return API_ERROR_INVALID_XFER_PTR;
		}
		/* 1 byte */
		address |= AD9508_SPI_1BYTE;
		inData[0] = ((address >> 8) & 0xFF);
		inData[1] = ((address >> 0) & 0xFF);
		inData[2] = data;
		err = h->dev_xfer(h->user_data, inData, outData, AD9508_IN_OUT_BUFF_SZ);
	}
	return err;
}

static int spi_configure(ad9508_handle_t *h)
{
	switch (h->sdo)
	{
	case SPI_SDO:
		return ad9508_register_write(h, AD9508_REG_SPI_CTL, 0x18);
	case SPI_SDIO:
		return ad9508_register_write(h, AD9508_REG_SPI_CTL, 0x00);
	default:
		return API_ERROR_SPI_SDO;
		break;
	}
	return API_ERROR_SPI_SDO;
}


ADI_API int ad9508_init(ad9508_handle_t *h)
{
	if (h != INVALID_POINTER)
	{
		if (h->dev_xfer == INVALID_POINTER)
		{
			return API_ERROR_INVALID_XFER_PTR;
		}
		if (h->sdo >= SPI_CONFIG_MAX)
		{
			return API_ERROR_SPI_SDO;
		}
		return spi_configure(h);
	}
	return API_ERROR_INVALID_HANDLE_PTR;
}

ADI_API int ad9508_reset(ad9508_handle_t *h)
{	
	uint8_t reg;
	if (h != INVALID_POINTER)
	{
		if (h->dev_xfer == INVALID_POINTER)
		{
			return API_ERROR_INVALID_XFER_PTR;
		}
		reg = AD9508_SPI_CTL_SOFT_RESET | AD9508_SPI_CTL_SDO_EN;
		return ad9508_register_write(h, AD9508_REG_SPI_CTL, reg);
	}
	return API_ERROR_INVALID_HANDLE_PTR;
}

