/**
 * \brief Contains init settings DAC reference handles
 *
 */
#include "app_clk_cfg.h"
#include "app_error.h"

#include <stdio.h>
#include "api_errors.h"
#include "adf4355.h"
#include "ad9508.h"
#include "api_def.h"
#include "hal.h"

#define FALSE 0x0
#define TRUE  0x1
#define APP_REF_CLK_FREQ_MHZ 120

uint32_t clk_reg_2G[ADF4355_NUM_REGS] =  {
    0x00200850,
    0x05555551,
    0x002801E2,
    0x00000003,
    0x30020984,
    0x00800025,
    0x35206676,
    0x12000007,
    0x102D0428,
    0x0C0C7FF9,
    0x00C012FA,
    0x0061300B,
    0x0000041C
};

uint32_t clk_reg_5G[ADF4355_NUM_REGS] =  {
    // CONFIG FROM ANALOG
    // 0x00200A60,
    // 0x0AAAAAA1,
    // 0x005001D2,
    // 0x00000003,
    // 0x30020984,
    // 0x00800025,
    // 0x35006676,
    // 0x12000007,
    // 0x102D0428,
    // 0x0C0C7FF9,
    // 0x0C0C12FA,
    // 0x0061300B,
    // 0x0000041C

    // OWN CONFIG
    0x00200530,
    0x05555551,
    0x00040032,
    0x00000003,
    0x32008B84,
    0x00800025,
    0x350183F6,//0x350180F6,
    0x12000007,
    0x102D0428,
    0x19193CC9,
    0x00C026BA,
    0x0061300B,
    0x0001041C
};

uint32_t clk_reg_6G[ADF4355_NUM_REGS] =  {

    0x00200C80,
    0x00000001,
    0x000001E2,
    0x00000003,
    0x30020984,
    0x00800025,
    0x35006276,
    0x12000007,
    0x102D0428,
    0x0C0C7FF9,
    0x0C0C12FA,
    0x0061300B,
    0x0000041C
};

adf4355_handle_t clk_h =
{
    0,
    &spi_xfer_adf4355,   /* Clk  SPI function */
    {0}
};

ad9508_handle_t buff_h =
{
   0,
   SPI_SDIO,
   &spi_xfer_ad9508,
   &delay_us
};


int clk_init()
{
    int appError = APP_ERR_OK;
    int clkError = API_ERROR_OK;
    int buffError = API_ERROR_OK;


    /*Initialise clk Module*/
    clkError = adf4355_init(&clk_h);
    if(clkError != API_ERROR_OK)
    {
        return APP_ERR_CLK_FAIL;
    }
    /*Initialise Buffer Module*/
    buffError = ad9508_init(&buff_h);
    if(buffError != API_ERROR_OK)
    {
       return APP_ERR_BUFF_FAIL;
    }
    return appError;
}

int clk_shutdown()
{
    int appError = APP_ERR_OK;
    int clkError = API_ERROR_OK;
    int buffError = API_ERROR_OK;

    /*Reset Buff Module*/
    buffError = ad9508_reset(&buff_h);
    if(buffError != API_ERROR_OK)
    {
       return APP_ERR_BUFF_FAIL;
    }

    /*Reset Clk Module*/
    clkError = adf4355_init(&clk_h);
    if(clkError != API_ERROR_OK)
    {
        return APP_ERR_CLK_FAIL;
    }

    return appError;
}

int clk_set_mode(int ref_clk_mhz, int dac_rate_mhz, int lane_rate_mhz)
{
    int appError = APP_ERR_OK;
    int clkError = API_ERROR_OK;
    int buffError = API_ERROR_OK;
    int i = 0;
    int div_ratio_tmp = 0x3;
    uint8_t div_ratio = 0x0; 
    printf("CLK:DAC RATE: %d MHz \r\n", dac_rate_mhz);   
    /*Configure clk Modules for desired Frequency*/
    switch(dac_rate_mhz)
    {
        case 2000:
            for (i=0; i < ADF4355_NUM_REGS; i++)
            {
                 clk_h.reg[i] = clk_reg_2G[i];
            }
            break;
        case 5000:
            for (i=0; i<ADF4355_NUM_REGS; i++)
            {
                 clk_h.reg[i] = clk_reg_5G[i];
            }
            break;
        case 6000:
            for (i=0; i<ADF4355_NUM_REGS; i++)
            {
                 clk_h.reg[i] = clk_reg_6G[i];
            }
            break;
        default:
            printf("CLK:Mode: Not yet Supported\r\n");
    }

    clkError = adf4355_update_registers(&clk_h);
    delay_us(100);
    /*Configure Buffer*/
    printf("CLK:Mode: Disable Output for 1,2\r\n");
    printf("CLK:Mode: Enable Output 0 for GBT_CLK_M2C on the FMC connector (DIV8) 156,250MHz \r\n");
    printf("CLK:Mode: Enable Output 3 for AD9161, Sysref\r\n");
    // Control regs

    // output 0 (GBT_CLK_M2C on the FMC connector)
    buffError = ad9508_register_write(&buff_h, 0x15, 0x07); // set the divide ration to 8
    // output 1 (Not connected)
    buffError = ad9508_register_write(&buff_h, 0x1F, 0x94); // power down this output
    // output 2 (SYSREF2 on the FMC connector)
    buffError = ad9508_register_write(&buff_h, 0x25, 0x94); // power down this output
    // output 3 (AD9161 SYSREF)
    buffError = ad9508_register_write(&buff_h, 0x27, 0x01); // set the divide ratio to 1 for now

    div_ratio_tmp = ( (dac_rate_mhz*10)/lane_rate_mhz);
    div_ratio = (uint8_t) (div_ratio_tmp -1);
    printf("CLK:Mode: Div Ratio final: %d\r\n", div_ratio);
    delay_us(15000);
    buffError = ad9508_register_write(&buff_h, 0x27, div_ratio); // set the divide ratio that will be used from now on

    if(clkError != API_ERROR_OK)
    {
        appError = APP_ERR_CLK_FAIL;
    }

    if(buffError != API_ERROR_OK)
    {
        appError = APP_ERR_BUFF_FAIL;
    }
    return appError;
}

int clk_check_config(void)
{
   int appError = APP_ERR_OK;
   int buffError = API_ERROR_OK;
#if 0
   uint8_t reg_data =0xFF;


   buffError = ad9508_register_read(&buff_h, 0x00, &reg_data);
   printf("AD9508 init 0x00: %x \r\n", reg_data); 

   buffError = ad9508_register_read(&buff_h, 0x15, &reg_data);
   printf("AD9508 init 0x15: %x \r\n", reg_data); 

   buffError = ad9508_register_read(&buff_h, 0x1F, &reg_data);
   printf("AD9508 init 0x1F: %x \r\n", reg_data); 

   buffError = ad9508_register_read(&buff_h, 0x25, &reg_data);
   printf("AD9508 init 0x25: %x \r\n", reg_data);

   buffError = ad9508_register_read(&buff_h, 0x2B, &reg_data);
   printf("AD9508 init 0x2B: %x \r\n", reg_data); 

#endif
 
    if(buffError != API_ERROR_OK)
    {

     appError = APP_ERR_BUFF_FAIL;

    }
    return appError;
}
