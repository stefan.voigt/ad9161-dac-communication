#include "../../../API/include/api_errors.h"
#include <stdint.h>
#include "adf4355.h"

#define ADF4355_IN_OUT_BUFF_SZ 4

int adf4355_update_registers(adf4355_handle_t *h)
{
	int err = API_ERROR_INVALID_HANDLE_PTR;
	int idx = 0;
	uint8_t inData[ADF4355_IN_OUT_BUFF_SZ];
	uint8_t outData[ADF4355_IN_OUT_BUFF_SZ];

	if (h != INVALID_POINTER)
	{
		if (h->dev_xfer == INVALID_POINTER)
		{
			return API_ERROR_INVALID_XFER_PTR;
		}

		for (idx = ADF4355_NUM_REGS - 1; idx >= 0; idx--)
		{
			inData[0] = ((h->reg[idx] >> 24) & 0xFF);
			inData[1] = ((h->reg[idx] >> 16) & 0xFF);
			inData[2] = ((h->reg[idx] >> 8) & 0xFF);
			inData[3] = ((h->reg[idx] >> 0) & 0xFF);
			err = h->dev_xfer(h->user_data, inData, outData, ADF4355_IN_OUT_BUFF_SZ);
			if (err != API_ERROR_OK)
			{
				return err;
			}
		}
	}
	return err;
}

int adf4355_init(adf4355_handle_t *h)
{	
	if (h != INVALID_POINTER)
	{
		if (h->dev_xfer == INVALID_POINTER)
		{
			return API_ERROR_INVALID_XFER_PTR;
		}
		/* Reserved registers/fields not supposed to change*/
		h->reg[0] = 0x00;
		h->reg[1] = 0x01;
		h->reg[2] = 0x02;
		h->reg[3] = 0x03;
		h->reg[4] = 0x04;
		h->reg[5] = (0x80002 << 4) | 0x05;
		h->reg[6] = (5u << 26) | 0x06;
		h->reg[7] = (1u << 28) | 0x07;
		h->reg[8] = (0x102D042 << 4) | 0x08;
		h->reg[9] = 0x09;
		h->reg[10] = (0x3 << 22) | 0x0A;
		h->reg[11] = (0x61300 << 4) | 0x0B;
		h->reg[12] = (0x41 << 4) | 0x0C;
		return API_ERROR_OK;
	}
	return API_ERROR_INVALID_HANDLE_PTR;
}
