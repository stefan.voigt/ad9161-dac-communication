#ifndef __ADF4355_H__
#define __ADF4355_H__

#include <stdint.h>

#define ADF4355_NUM_REGS 13

typedef int(*spi_xfer_t)(void *user_data, uint8_t *indata, uint8_t *outdata, int size_bytes);

typedef struct
{
	void *user_data;
	spi_xfer_t dev_xfer;
	uint32_t reg[ADF4355_NUM_REGS];
}adf4355_handle_t;


int adf4355_init(adf4355_handle_t *h);
int adf4355_update_registers(adf4355_handle_t *h);

#endif /* !__ADF4355_H__ */
