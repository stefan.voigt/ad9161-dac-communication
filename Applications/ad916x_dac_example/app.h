/**
 * \file app.h
 *
 * \brief Contains Highlevel Application Interface 
 *
 */

#ifndef APP_H_
#define APP_H_

#include <stdint.h>

int APP_init(void);
int APP_shutdown(void);
int APP_set_jesd_mode(int mode);
int APP_set_nco_only_mode(uint64_t carrier_freq_mhz, uint16_t amplitude);
int APP_set_nco_shift(uint64_t carrier_freq_mhz);
int APP_enable_mode(void);
int APP_enable_mode_repeated_ILAS(void);
int APP_PRBS_checker(void);
int APP_CheckLink(void);
int APP_check_mode(void);
int APP_main(void);
int APP_reset(void);
#endif
