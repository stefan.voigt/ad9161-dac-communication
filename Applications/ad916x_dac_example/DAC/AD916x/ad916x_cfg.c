/**
 * \brief Contains init settings DAC reference handles
 *
 */
#include "app_dac_cfg.h"
#include "app_error.h"

#include <stdio.h>
#include "api_errors.h"
#include "AD916x.h"
#include "api_def.h"
#include "hal.h"

#define FALSE 0x0
#define TRUE  0x1

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0') 


ad916x_handle_t dac_h = {
	0,                  /* No Client App Userdata */
	SPI_SDO,            /* DAC App to use 4 Wire SPI config */
	5000000000,         /* DAC Clk Frequency */
	&spi_xfer_ad916x,   /* DAC App SPI function */
	&delay_us,          /* DAC App Delay Function */
	&dac_event_handler, /* DAC App Event Handler */
	0,                  /* No TX Enable Pin Control */
	0,                  /* NO RESETB Pin Control */
	0,                  /* NO HW Open Function */
	0                   /* NO HW close Function */
};

int dac_init(void* dac_h)
{
	printf("AD916x: dac_init called\r\n");

	int appError = APP_ERR_OK;
	int dacError = API_ERROR_OK;
	uint8_t revision[3] = {0,0,0};
	ad916x_chip_id_t dac_chip_id;

	if (dac_h == NULL) {
		return (APP_ERR_DAC_FAIL);
	}
	
	ad916x_handle_t *ad916x_h = dac_h;

	/*Initialise DAC Module*/
	dacError = ad916x_init(ad916x_h);
	if (dacError != API_ERROR_OK) {
		return APP_ERR_DAC_FAIL;
	}
	printf("ad916x_init okay\r\n");
	
	dacError = ad916x_get_chip_id(ad916x_h, &dac_chip_id);
	if (dacError != API_ERROR_OK) {
		return APP_ERR_DAC_FAIL;
	}

	dacError = ad916x_get_revision(ad916x_h,&revision[0],&revision[1],&revision[2]);
	if (dacError !=API_ERROR_OK) {
		return APP_ERR_DAC_FAIL;
	}

	printf("*********************************************\r\n");
	printf("AD916x DAC Chip ID: %d \r\n", dac_chip_id.chip_type);
	printf("AD916x DAC Product ID: %x \r\n", dac_chip_id.prod_id);
	printf("AD916x DAC Product Grade: %d \r\n", dac_chip_id.prod_grade);
	printf("AD916x DAC Product Revision: %d \r\n", dac_chip_id.dev_revision);
	printf("AD916x Revision: %d.%d.%d \r\n", revision[0], revision[1], revision[2]);
	printf("*********************************************\r\n");

	return appError;
}

int dac_reset(void* dac_h)
{
	int appError = APP_ERR_OK;
	int dacError = API_ERROR_OK;

	if (dac_h == NULL) {
		return APP_ERR_DAC_FAIL;
	}
	ad916x_handle_t *ad916x_h = dac_h;

	/*Initialise DAC Module*/
	printf("reset dac \r\n");
	dacError = ad916x_reset(ad916x_h, 0);
	if(dacError != API_ERROR_OK) {
		return APP_ERR_DAC_FAIL;
	}

	return appError;
}

int dac_shutdown(void* dac_h)
{
	int appError = APP_ERR_OK;
	int dacError = API_ERROR_OK;

	if (dac_h == NULL) {
		return APP_ERR_DAC_FAIL;
	}
	ad916x_handle_t *ad916x_h = dac_h;

	/*Initialise DAC Module*/
	dacError = ad916x_reset(ad916x_h, 0);
	dacError = ad916x_deinit(ad916x_h);
	if(dacError != API_ERROR_OK) {
		return APP_ERR_DAC_FAIL;
	}

	return appError;
}

int dac_set_mode(void* dac_h, DAC_MODE dac_mode, int dac_rate_mhz, void *mode_data)
{
	int appError = APP_ERR_OK;
	int dacError = API_ERROR_OK;
	uint64_t jesdLaneRate = 0x0;
	uint64_t dac_clk_hz = 0x0;
	dac_jesd_data *jesd_data;
	dac_nco_data *nco_data;
	jesd_param_t appJesdConfig = { 8, 1, 2, 4, 1,32,16,16,0,0, 0,0,0,0};
	uint8_t pll_lock_status = 0x0;
	ad916x_event_t jesd_event_list[EVENT_NOF_EVENTS] = { 
		EVENT_SYSREF_JITTER,
		EVENT_DATA_RDY,
		EVENT_JESD_LANE_FIFO_ERR,
		EVENT_JESD_PRBS_IMG_ERR,
		EVENT_JESD_PRBS_REAL_ERR,
		EVENT_JESD_NOT_IN_TBL_ERR,
		EVENT_JESD_K_ERR,
		EVENT_JESD_ILD_ERR,
		EVENT_JESD_ILS_ERR,
		EVENT_JESD_CKSUM_ERR,
		EVENT_JESD_FS_ERR,
		EVENT_JESD_CGS_ERR,
		EVENT_JESD_ILAS_ERR};

	if(dac_h == NULL) {
		return APP_ERR_DAC_FAIL;
	}
	ad916x_handle_t *ad916x_h = dac_h;

	if(mode_data == NULL) {
		return APP_ERR_DAC_FAIL;
	}

	/*Configure DAC Modules for desired Mode*/
	switch(dac_mode) {
	case DAC_JESD_MODE:
		printf("DAC:Mode: Configuring JESD Mode\r\n");
		dac_clk_hz = (((uint64_t)(dac_rate_mhz))*1000000);
		dacError = ad916x_dac_set_clk_frequency(ad916x_h, dac_clk_hz);
		dacError = ad916x_set_events(ad916x_h, &jesd_event_list[0], 4, 0x1);
		jesd_data = (dac_jesd_data*) mode_data;
		appJesdConfig.jesd_L = jesd_data->L;
		appJesdConfig.jesd_M = jesd_data->M;
		appJesdConfig.jesd_F = jesd_data->F;
		appJesdConfig.jesd_S = jesd_data->S;
		appJesdConfig.jesd_HD = ((appJesdConfig.jesd_F == 1) ? 1:0);

		dacError = ad916x_jesd_config_datapath(ad916x_h, appJesdConfig,
                                         jesd_data->interpol, &jesdLaneRate);
		printf("DAC:MODE:JESD: Jesd Lane Rate : %lld MHz\r\n", jesdLaneRate);
		if (dacError != API_ERROR_OK) {
			printf("DAC:MODE:ERROR: JESD Invalid Configuration \r\n");
			break;
		}
		printf("DAC:MODE:JESD: Configured \r\n");

		dacError = ad916x_jesd_enable_datapath(ad916x_h, 0xFF, 0x1, 0x1);
		if (dacError == API_ERROR_OK) {
			printf("DAC:MODE:JESD: interface enabled \r\n");
		} else {
			break;
		}
		delay_us(200000);

		dacError = ad916x_jesd_get_pll_status(ad916x_h, &pll_lock_status);
		if (dacError == API_ERROR_OK) {
			printf("DAC:MODE:JESD: Serdes PLL stat: 0b"BYTE_TO_BINARY_PATTERN,BYTE_TO_BINARY(pll_lock_status));printf("\r\n");
			//printf("DAC:MODE:JESD: Serdes PLL stat: %x \r\n", pll_lock_status);
		}
		break;
	case DAC_NCO_ONLY_MODE:
		printf("DAC:Mode: Configuring NCO  Mode\r\n");
		nco_data = (dac_nco_data*) mode_data;
		dac_clk_hz = (((uint64_t)(dac_rate_mhz))*1000000);
		dacError = ad916x_dac_set_clk_frequency(ad916x_h, dac_clk_hz);
		dacError = ad916x_nco_set(ad916x_h, 0, nco_data->carrier_freq, nco_data->amplitude, 0x1);
		if (dacError != API_ERROR_OK) {
			break;
		}
		printf("DAC:MODE:NCO ONLY: Configured \r\n");
		break;
	case DAC_NCO_SHIFT:
		printf("DAC:Mode: Configuring NCO SHIFT Mode\r\n");
		nco_data = (dac_nco_data*) mode_data;
		dacError = ad916x_nco_set(ad916x_h, 0, nco_data->carrier_freq, nco_data->amplitude, 0x0);
		if (dacError != API_ERROR_OK) {
			break;
		}
		printf("DAC:MODE:NCO shift: Configured \r\n");
		break;
	default:
		printf("DAC:Mode: Unknown Mode\r\n");
	}

	if (dacError != API_ERROR_OK) {
		appError = APP_ERR_DAC_FAIL;
	}

	return appError;
}

int dac_enable_mode(void* dac_h)
{
	int appError = APP_ERR_OK;
	int dacError = API_ERROR_OK;
	uint8_t pll_lock_status = 0x0;
	ad916x_jesd_link_stat_t app_dac_link_status;

	if (dac_h == NULL) {
		return APP_ERR_DAC_FAIL;
	}
	ad916x_handle_t *ad916x_h = dac_h;

	/*Check PLL Status */
	dacError = ad916x_jesd_get_pll_status(ad916x_h, &pll_lock_status);
	if (dacError != API_ERROR_OK) {
		return APP_ERR_DAC_FAIL;
	}
	printf("DAC:MODE:JESD: Serdes PLL stat: 0b"BYTE_TO_BINARY_PATTERN,BYTE_TO_BINARY(pll_lock_status));printf("\r\n");
	//printf("DAC:MODE:JESD: Serdes PLL stat: %x \r\n", pll_lock_status);
	if (!(pll_lock_status & 0x1)) {
		printf("DAC:MODE:JESD: ERROR :Serdes PLL NOT Locked \r\n");
		return APP_ERR_DAC_FAIL;
	}
	printf("DAC:MODE:JESD: Sedes PLL locked!\r\n");

	/*Enable Link*/
	dacError = ad916x_jesd_enable_link(ad916x_h, 0x1);
	if (dacError != API_ERROR_OK) {
		printf("DAC:MODE:JESD: ERROR : Enable Link failed \r\n");
		return APP_ERR_DAC_FAIL;
	}
	printf("DAC:MODE:JESD: Bring up started checking Link... \r\n");
	dacError = ad916x_jesd_get_link_status(ad916x_h, &app_dac_link_status);
	if (dacError != API_ERROR_OK) {
		printf("DAC:MODE:JESD: ERROR : Get Link status failed \r\n");
		return APP_ERR_DAC_FAIL;
	}

	// printf("code_grp_sync: %x \r\n", app_dac_link_status.code_grp_sync_stat);
	// printf("frame_sync_stat: %x \r\n", app_dac_link_status.frame_sync_stat);
	// printf("good_checksum_stat: %x \r\n", app_dac_link_status.good_checksum_stat);
	// printf("init_lane_sync_stat: %x \r\n", app_dac_link_status.init_lane_sync_stat);

	printf("code_grp_sync      : 0b"BYTE_TO_BINARY_PATTERN,BYTE_TO_BINARY(app_dac_link_status.code_grp_sync_stat));printf("\r\n");
	printf("frame_sync_stat    : 0b"BYTE_TO_BINARY_PATTERN,BYTE_TO_BINARY(app_dac_link_status.frame_sync_stat));printf("\r\n");
	printf("good_checksum_stat : 0b"BYTE_TO_BINARY_PATTERN,BYTE_TO_BINARY(app_dac_link_status.good_checksum_stat));printf("\r\n");
	printf("init_lane_sync_stat: 0b"BYTE_TO_BINARY_PATTERN,BYTE_TO_BINARY(app_dac_link_status.init_lane_sync_stat));printf("\r\n");

	printf("############# TODO: CHECK TX ENABLE BIT IN HARDWARE! ################\r\n");
	// dacError = ad916x_transmit_enable(ad916x_h, TX_ENABLE);
	if (dacError != API_ERROR_OK) {
		printf("DAC:MODE:JESD: ERROR : TX ENABLE ERROR \r\n");
		return APP_ERR_DAC_FAIL;
	}

	return appError;
}

int dac_enable_mode_repeated_ILAS(void* dac_h)
{
	int appError = APP_ERR_OK;
	int dacError = API_ERROR_OK;
	uint8_t pll_lock_status = 0x0;
	ad916x_jesd_link_stat_t app_dac_link_status;

	if (dac_h == NULL) {
		return APP_ERR_DAC_FAIL;
	}
	ad916x_handle_t *ad916x_h = dac_h;

	/*Check PLL Status */
	dacError = ad916x_jesd_get_pll_status(ad916x_h, &pll_lock_status);
	if (dacError != API_ERROR_OK) {
		return APP_ERR_DAC_FAIL;
	}
	printf("DAC:MODE:JESD: Serdes PLL stat: 0b"BYTE_TO_BINARY_PATTERN,BYTE_TO_BINARY(pll_lock_status));printf("\r\n");
	//printf("DAC:MODE:JESD: Serdes PLL stat: %x \r\n", pll_lock_status);
	if (!(pll_lock_status & 0x1)) {
		printf("DAC:MODE:JESD: ERROR :Serdes PLL NOT Locked \r\n");
		return APP_ERR_DAC_FAIL;
	}
	printf("DAC:MODE:JESD: Sedes PLL locked!\r\n");

	/*Enable Link in repeated ILAS test mode*/
	dacError = ad916x_jesd_enable_link_repeated_ILAS(ad916x_h, 0x1);
	if (dacError != API_ERROR_OK) {
		printf("DAC:MODE:JESD: ERROR : Enable Link failed \r\n");
		return APP_ERR_DAC_FAIL;
	}
	printf("DAC:MODE:JESD: Bring up started checking Link... \r\n");
	dacError = ad916x_jesd_get_link_status(ad916x_h, &app_dac_link_status);
	if (dacError != API_ERROR_OK) {
		printf("DAC:MODE:JESD: ERROR : Get Link status failed \r\n");
		return APP_ERR_DAC_FAIL;
	}

	printf("############# TODO: CHECK TX ENABLE BIT IN HARDWARE! ################\r\n");
	// dacError = ad916x_transmit_enable(ad916x_h, TX_ENABLE);
	if (dacError != API_ERROR_OK) {
		printf("DAC:MODE:JESD: ERROR : TX ENABLE ERROR \r\n");
		return APP_ERR_DAC_FAIL;
	}

	return appError;
}

int dac_monitor(void* dac_h)
{
	int dacError = API_ERROR_OK;
	uint8_t dac_int_pending = 0x0;
	uint8_t dac_int_status[3];
	if (dac_h == NULL) {
		return APP_ERR_DAC_FAIL;
	}
	ad916x_handle_t *ad916x_h = dac_h;

	/*Check interrupt Status*/
	dacError = ad916x_get_interrupt_status(ad916x_h, &dac_int_pending, &dac_int_status[0]);
	if (dacError != API_ERROR_OK) {
		printf("DAC:MONITOR: ERROR: Get Interrupt status failed \r\n");
		return APP_ERR_DAC_FAIL;
	}

	if (dac_int_pending) {
		printf("DAC:MONITOR: Interrupt Pending \r\n");
		dacError = ad916x_isr(dac_h);
		if (dacError != API_ERROR_OK) {
			printf("DAC:MONITOR:ERROR: ISR fail \r\n");
			return APP_ERR_DAC_FAIL;
		}
	} else {
		printf("DAC:MONITOR: No interrupt detected \r\n");
	}

	return APP_ERR_OK;
}
int dac_event_handler(uint16_t event, uint8_t ref, void* data)
{
	int appError  = APP_ERR_OK;

	switch(event) {
	case EVENT_SYSREF_JITTER:
		printf("APP DAC: Sysref Jitter Event Occurred\r\n");
		break;
	case EVENT_DATA_RDY:
		printf("APP DAC: Data Ready Event Occurred\r\n");
		break;
	case EVENT_JESD_LANE_FIFO_ERR:
		printf("APP DAC: JESD LANE FIFO Error Event Occurred\r\n");
		break;
	case EVENT_JESD_PRBS_IMG_ERR:
		printf("APP DAC: JESD PRBS Q Error Event Occurred\r\n");
		break;
	case EVENT_JESD_PRBS_REAL_ERR:
		printf("APP DAC: JESD PRBS I Error Event Occurred\r\n");
		break;
	case EVENT_JESD_ILAS_ERR:
		printf("APP DAC: JESD ILAS Error Event Occurred\r\n");
		break;
	case EVENT_JESD_BAD_DISPARITY_ERR:
		printf("APP DAC: JESD BDE on Lane %d, Event Occurred\r\n", ref);
		break;
	case EVENT_JESD_NOT_IN_TBL_ERR:
		printf("APP DAC: JESD NIT ERR on Lane %d, Event Occurred\r\n", ref);
		break;
	case EVENT_JESD_K_ERR:
		printf("APP DAC: JESD K ERR on Lane %d, Event Occurred\r\n", ref);
		break;
	case EVENT_JESD_ILD_ERR:
		printf("APP DAC: JESD ILD ERR on Lane %d, Event Occurred\r\n", ref);
		break;
	case EVENT_JESD_ILS_ERR:
		printf("APP DAC: JESD ILS ERR on Lane %d, Event Occurred\r\n", ref);
		break;
	case EVENT_JESD_CKSUM_ERR:
		printf("APP DAC: JESD CKS ERR on Lane %d, Event Occurred\r\n", ref);
		break;
	case EVENT_JESD_FS_ERR:
		printf("APP DAC: JESD FS ERR on Lane %d, Event Occurred\r\n", ref);
		break;
	case EVENT_JESD_CGS_ERR:
		printf("APP DAC: JESD ILD ERR on Lane %d, Event Occurred\r\n", ref);
		break;
	default:
		printf("APP DAC: ERR: UKNOWN Event Occurred\r\n");
	}

	return appError;
}

/*debug function*/
int dac_check_int_config(void* dac_h)
{
	uint8_t reg_data = 0x88;
	int appError  = APP_ERR_OK;
	int dacError = API_ERROR_OK;
	if(dac_h == NULL) {
		return APP_ERR_DAC_FAIL;
	}

	ad916x_handle_t *ad916x_h = dac_h;
	dacError = ad916x_register_read(ad916x_h, 0x020, &reg_data);
	if (dacError == API_ERROR_OK) {
		printf("IRQ_EN 0x20: %x \r\n", reg_data);
	}

	dacError = ad916x_register_read(ad916x_h, 0x4B8, &reg_data);
	if (dacError == API_ERROR_OK) {
		printf("JESD LINK IRQ EN 1 0x4B8: %x \r\n", reg_data);
	}

	dacError = ad916x_register_read(ad916x_h, 0x4B9, &reg_data);
	if (dacError == API_ERROR_OK) {
		printf("JESD LINK IRQ EN 2  0x4B9: %x \r\n", reg_data);
	}

	return appError;
}

int dac_check_nco_config(void* dac_h)
{
	uint8_t reg_data = 0x88;
	int appError  = APP_ERR_OK;
	int dacError = API_ERROR_OK;
	if(dac_h == NULL) {
		return APP_ERR_DAC_FAIL;
	}

	ad916x_handle_t *ad916x_h = dac_h;
	dacError = ad916x_register_read(ad916x_h, 0x110, &reg_data);
	printf("0x110: %x \r\n", reg_data);
	dacError = ad916x_register_read(ad916x_h, 0x111, &reg_data);
	printf("0x111: %x \r\n", reg_data);
	dacError = ad916x_register_read(ad916x_h, 0x150, &reg_data);
	printf("0x150: %x \r\n", reg_data);

	dacError = ad916x_register_read(ad916x_h, 0x14E, &reg_data);
	printf("0x14E: %x \r\n", reg_data);
	dacError = ad916x_register_read(ad916x_h, 0x14F, &reg_data);
	printf("0x14F: %x \r\n", reg_data);
	dacError = ad916x_register_read(ad916x_h, 0x119, &reg_data);
	printf("0x119: %x \r\n", reg_data);
	dacError = ad916x_register_read(ad916x_h, 0x118, &reg_data);
	printf("0x118: %x \r\n", reg_data);
	dacError = ad916x_register_read(ad916x_h, 0x117, &reg_data);
	printf("0x117: %x \r\n", reg_data);
	dacError = ad916x_register_read(ad916x_h, 0x116, &reg_data);
	printf("0x116: %x \r\n", reg_data);
	dacError = ad916x_register_read(ad916x_h, 0x115, &reg_data);
	printf("0x115: %x \r\n", reg_data);
	dacError = ad916x_register_read(ad916x_h, 0x114, &reg_data);
	printf("0x114: %x \r\n", reg_data);

	dacError = ad916x_register_read(ad916x_h, 0x113, &reg_data);
	printf("0x113: %x \r\n", reg_data);
	return appError;
}
int dac_check_init(void* dac_h)
{

    uint8_t reg_data = 0x88;
    int appError  = APP_ERR_OK;
    int dacError = API_ERROR_OK;

    if(dac_h == NULL)
    {
        return APP_ERR_DAC_FAIL;
    }
    ad916x_handle_t *ad916x_h = dac_h;

    printf("DAC:INIT:Checking Registers \r\n");
    dacError = ad916x_register_read(ad916x_h, 0x00, &reg_data);
    printf("read reg test 0x00: %x \r\n", reg_data);
    dacError = ad916x_register_read(ad916x_h, 0x3, &reg_data);
    printf("chip ID 0x03: %x \r\n", reg_data);
    dacError = ad916x_register_read(ad916x_h, 0x4, &reg_data);
    printf("chip ID 0x04: %x \r\n", reg_data);
    dacError = ad916x_register_read(ad916x_h, 0x5, &reg_data);
    printf("chip ID 0x05: %x \r\n", reg_data);
#if 0
    dacError = ad916x_register_read(ad916x_h, 0x58, &reg_data);
    printf("DAC ADI REC 0x58: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x90, &reg_data);
    printf("DAC ADI REC 0x90: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x80, &reg_data);
    printf("DAC ADI REC 0x80: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x40, &reg_data);
    printf("DAC ADI REC 0x40: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x47, &reg_data);
    printf("DAC ADI REC 0x47: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x49, &reg_data);
    printf("DAC ADI REC 0x49: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x4A, &reg_data);
    printf("DAC ADI REC 0x4A: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x4E, &reg_data);
    printf("DAC ADI REC 0x4E: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x59, &reg_data);
    printf("DAC ADI REC 0x59: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x5A, &reg_data);
    printf("DAC ADI REC 0x5A: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x79, &reg_data);
    printf("DAC ADI REC 0x79: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x93, &reg_data);
    printf("DAC ADI REC 0x93: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x94, &reg_data);
    printf("DAC ADI REC 0x94: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x96, &reg_data);
    printf("DAC ADI REC 0x96: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x9E, &reg_data);
    printf("DAC ADI REC 0x9E: %x \r\n", reg_data);
#endif
    if(dacError != API_ERROR_OK)
    {
        printf("DAC:INIT:Check Registers: SPI Error\r\n");
    }

    return appError;

}
int dac_check_jesd_config(void* dac_h)
{

    uint8_t reg_data = 0x88;
    int appError  = APP_ERR_OK;
    int dacError = API_ERROR_OK;
    ad916x_jesd_link_stat_t app_dac_link_status;

    if(dac_h == NULL)
    {
        return APP_ERR_DAC_FAIL;
    }
    ad916x_handle_t *ad916x_h = dac_h;
    dacError = ad916x_jesd_get_link_status(ad916x_h, &app_dac_link_status);
    if (dacError == API_ERROR_OK)
    {
        printf("code_grp_sync: %x \r\n", app_dac_link_status.code_grp_sync_stat);
        printf("frame_sync_stat: %x \r\n", app_dac_link_status.frame_sync_stat);
        printf("good_checksum_stat: %x \r\n", app_dac_link_status.good_checksum_stat);
        printf("init_lane_sync_stat: %x \r\n", app_dac_link_status.init_lane_sync_stat);
    }

#if 1
    dacError = ad916x_register_read(ad916x_h, 0x58, &reg_data);
    printf("DAC ADI REC 0x58: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x90, &reg_data);
    printf("DAC ADI REC 0x90: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x80, &reg_data);
    printf("DAC ADI REC 0x80: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x40, &reg_data);
    printf("DAC ADI REC 0x40: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x289, &reg_data);
    printf("DAC ADI REC 0x289: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x20, &reg_data);
    printf("DAC ADI REC 0x20: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x4B8, &reg_data);
    printf("DAC ADI REC 0x4B8: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x4B9, &reg_data);
    printf("DAC ADI REC 0x4B9: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x47, &reg_data);
    printf("DAC ADI REC 0x47: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x49, &reg_data);
    printf("DAC ADI REC 0x49: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x4A, &reg_data);
    printf("DAC ADI REC 0x4A: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x4E, &reg_data);
    printf("DAC ADI REC 0x4E: %x \r\n", reg_data);
    dacError = ad916x_register_read(ad916x_h, 0x59, &reg_data);
    printf("DAC ADI REC 0x59: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x5A, &reg_data);
    printf("DAC ADI REC 0x94: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x79, &reg_data);
    printf("DAC ADI REC 0x79: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x480, &reg_data);
    printf("DAC ADI REC 0x480: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x481, &reg_data);
    printf("DAC ADI REC 0x481: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x482, &reg_data);
    printf("DAC ADI REC 0x482: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x483, &reg_data);
    printf("DAC ADI REC 0x483: %x \r\n", reg_data);


    dacError = ad916x_register_read(ad916x_h, 0x484, &reg_data);
    printf("DAC ADI REC 0x484: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x485, &reg_data);
    printf("DAC ADI REC 0x485: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x486, &reg_data);
    printf("DAC ADI REC 0x486: %x \r\n", reg_data);

   dacError = ad916x_register_read(ad916x_h, 0x487, &reg_data);
    printf("DAC ADI REC 0x487: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x93, &reg_data);
    printf("DAC ADI REC 0x93: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x94, &reg_data);
    printf("DAC ADI REC 0x94: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x96, &reg_data);
    printf("DAC ADI REC 0x96: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x9E, &reg_data);
    printf("DAC ADI REC 0x9E: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x91, &reg_data);
    printf("DAC ADI REC 0x91: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0xD2, &reg_data);
    printf("DAC ADI REC 0xD2: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0xE8, &reg_data);
    printf("read reg test 0xE8: %x \r\n", reg_data);
#endif


#if 1
    printf("DAC:JESD:Checking Registers \r\n");
    dacError = ad916x_register_read(ad916x_h, 0x152, &reg_data);
    printf("read reg test 0x152: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x110, &reg_data);
    printf("read reg test 0x110: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x230, &reg_data);
    printf("read 0x230: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x289, &reg_data);
    printf("read 0x289: %x \r\n", reg_data);
    dacError = ad916x_register_read(ad916x_h, 0x84, &reg_data);
    printf("read 0x084: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x200, &reg_data);
    printf("read reg 0x200: %x \r\n", reg_data);


    dacError = ad916x_register_read(ad916x_h, 0x201, &reg_data);
    printf("DAC ADI REC 0x201: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x284, &reg_data);
    printf("DAC ADI REC 0x284: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x285, &reg_data);
    printf("DAC ADI REC 0x285: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x286, &reg_data);
    printf("DAC ADI REC 0x286: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x287, &reg_data);
    printf("DAC ADI REC 0x287: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x28B, &reg_data);
    printf("DAC ADI REC 0x28B: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x2A7, &reg_data);
    printf("DAC ADI REC 0x2A7: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x2AE, &reg_data);
    printf("DAC ADI REC 0x2AE: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x206, &reg_data);
    printf("DAC ADI REC 0x206: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x28F, &reg_data);
    printf("DAC ADI REC 0x28F: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x201, &reg_data);
    printf("DAC ADI REC 0x291: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x290, &reg_data);
    printf("DAC ADI REC 0x290: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x2A0, &reg_data);
    printf("DAC ADI REC 0x2A0: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x2A4, &reg_data);
    printf("DAC ADI REC 0x2A4: %x \r\n", reg_data);
    dacError = ad916x_register_read(ad916x_h, 0x110, &reg_data);
    printf("read reg test 0x110: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x29E, &reg_data);
    printf("read reg test 0x29E: %x \r\n", reg_data);
    dacError = ad916x_register_read(ad916x_h, 0x2A0, &reg_data);
    printf("read reg test 0x2A0: %x \r\n", reg_data);
    dacError = ad916x_register_read(ad916x_h, 0x2A4, &reg_data);
    printf("read reg test 0x2A4: %x \r\n", reg_data);
    dacError = ad916x_register_read(ad916x_h, 0x29F, &reg_data);
    printf("read reg test 0x29F: %x \r\n", reg_data);
    dacError = ad916x_register_read(ad916x_h, 0x2AA, &reg_data);
    printf("read reg test 0x2AA: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x2AB, &reg_data);
    printf("read reg test 0x2AB: %x \r\n", reg_data);
    dacError = ad916x_register_read(ad916x_h, 0x2B1, &reg_data);
    printf("read reg test 0x2B1: %x \r\n", reg_data);
    dacError = ad916x_register_read(ad916x_h, 0x2B2, &reg_data);
    printf("read reg test 0x2B2: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x218, &reg_data);
    printf("read reg test 0x218: %x \r\n", reg_data);
    dacError = ad916x_register_read(ad916x_h, 0x232, &reg_data);
    printf("read reg test 0x2B2: %x \r\n", reg_data);
    dacError = ad916x_register_read(ad916x_h, 0x268, &reg_data);
    printf("read reg test 0x268: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x476, &reg_data);
    printf("read reg test 0x476: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x280, &reg_data);
    printf("read reg test 0x280: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x300, &reg_data);
    printf("read reg test 0x300: %x \r\n", reg_data);

#endif

#if 1

    dacError = ad916x_register_read(ad916x_h, 0x111, &reg_data);
    printf("read reg test 0x111: %x \r\n", reg_data);
    dacError = ad916x_register_read(ad916x_h, 0x113, &reg_data);
    printf("read reg test 0x113: %x \r\n", reg_data);
    dacError = ad916x_register_read(ad916x_h, 0x119, &reg_data);
    printf("read reg test 0x119: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x118, &reg_data);
    printf("read reg test 0x118: %x \r\n", reg_data);
    dacError = ad916x_register_read(ad916x_h, 0x117, &reg_data);
    printf("read reg test 0x117: %x \r\n", reg_data);
    dacError = ad916x_register_read(ad916x_h, 0x116, &reg_data);
    printf("read reg test 0x116: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x115, &reg_data);
    printf("read reg test 0x115: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x113, &reg_data);
    printf("read reg test 0x280: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x150, &reg_data);
    printf("read reg test 0x150: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x14F, &reg_data);
    printf("read reg test 0x14F: %x \r\n", reg_data);

    dacError = ad916x_register_read(ad916x_h, 0x14E, &reg_data);
    printf("read reg test 0x14E: %x \r\n", reg_data);
#endif


    if(dacError != API_ERROR_OK)
    {
       printf("DAC:JESD:Check Registers: SPI Error \r\n");
    }
   return appError;
}
